﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RocketCore
{
    [Serializable]
    class Order
    {
        private static long next_id; //в целях автоинкремента id заявки
        internal long OrderId { get; private set; }
        internal int UserId { get; private set; }
        internal decimal OriginalAmount { get; private set; }
        internal decimal ActualAmount { get; set; } //текущий объём заявки может изменяться
        internal decimal Rate { get; set; } //может изменяться в TS-заявках
        internal int FCSource { get; private set; }
        internal string ExternalData { get; private set; }
        internal DateTime DtMade { get; private set; }

        internal Order() //конструктор заявки по умолчанию
        {
            OrderId = 0;
            UserId = 0;
            OriginalAmount = 0m;
            ActualAmount = 0m;
            Rate = 0m;
            FCSource = 0;
            ExternalData = "";
            DtMade = new DateTime();
        }

        internal Order(int user_id, decimal original_amount, decimal actual_amount, decimal rate) //конструктор заявки
        {
            OrderId = ++next_id; //инкремент id предыдущей заявки
            UserId = user_id;
            OriginalAmount = original_amount;
            ActualAmount = actual_amount;
            Rate = rate;
            FCSource = 0;
            ExternalData = "";
            DtMade = DateTime.Now;
        }

        internal Order(int user_id, decimal original_amount, decimal actual_amount, decimal rate, int fc_source, string external_data) //конструктор заявки с внешними данными
        {
            OrderId = ++next_id; //инкремент id предыдущей заявки
            UserId = user_id;
            OriginalAmount = original_amount;
            ActualAmount = actual_amount;
            Rate = rate;
            FCSource = fc_source;
            ExternalData = external_data;
            DtMade = DateTime.Now;
        }
    }

    [Serializable]
    struct OrderBuf : IEquatable<OrderBuf>
    {
        internal decimal ActualAmount;
        internal decimal Rate;

        public OrderBuf(decimal actual_amount, decimal rate)
        {
            ActualAmount = actual_amount;
            Rate = rate;
        }

        public bool Equals(OrderBuf other)
        {
            if (this.ActualAmount != other.ActualAmount) return false;
            if (this.Rate != other.Rate) return false;

            return true;
        }

        public override int GetHashCode()
        {
            return 0; //добавить имплементацию, если понадобится
        }
    }
}
